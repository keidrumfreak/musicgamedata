﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("stage")]
    class Stage
    {
        [Key]
        [Column("stage_id")]
        public int Id { get; set; }

        [Column("music_id")]
        public int MusicId { get; set; }

        [Column("difficulty_id")]
        public int DifficultyId { get; set; }

        [Column("notes")]
        public int Notes { get; set; }

        [Column("bpm_default")]
        public decimal? DefaultBpm { get; set; }

        [Column("bpm_min")]
        public decimal? MinBpm { get; set; }

        [Column("bpm_max")]
        public decimal? MaxBpm { get; set; }

        [ForeignKey(nameof(MusicId))]
        public Music Music { get; set; }

        [ForeignKey(nameof(DifficultyId))]
        public Difficulty Difficulty { get; set; }

        public bool Equals (int modelId, int musicId,int difficultyId, int notes, decimal?[] bpm)
        {
            if (MusicId != musicId) return false;
            if (DifficultyId != difficultyId) return false;
            if (Notes != notes) return false;
            if (bpm[0] != DefaultBpm) return false;
            if (bpm[1] != MinBpm) return false;
            if (bpm[2] != MaxBpm) return false;
            return true;
        }
    }
}
