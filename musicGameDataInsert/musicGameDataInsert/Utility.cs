﻿using System;

namespace musicGameDataInsert
{
    static class Utility
    {
        public static decimal? StringToDecimal(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            else
            {
                return Convert.ToDecimal(input);
            }
        }

        public static DateTime? StringToDateTime(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            else
            {
                return Convert.ToDateTime(input);
            }
        }
    }
}
