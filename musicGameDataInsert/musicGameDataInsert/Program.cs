﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicGameDataInsert.DataEntity;
using musicGameDataInsert.RecordModel;

namespace musicGameDataInsert
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileList = getFileList();
            foreach (var path in fileList)
            {
                dataProcess(path);
            }
        }

        static string[] getFileList()
        {
            var list = new List<string>();

            using (var reader = new StreamReader(File.OpenRead("fileList.txt")))
            {
                while (!reader.EndOfStream)
                {
                    list.Add(reader.ReadLine());
                }
            }

            return list.ToArray();
        }

        static void dataProcess(string filePath)
        {
            var rows = new List<string>();

            using (var reader = new StreamReader(File.OpenRead(filePath)))
            {
                while (!reader.EndOfStream)
                {
                    rows.Add(reader.ReadLine());
                }
            }

            RecordAgent agent = null;

            using (var entities = new MusicGameData())
            {
                foreach (var row in rows.Select((v, i) => new { v, i }))
                {
                    var record = row.v.Split(new char[] { '\t' }, StringSplitOptions.None);

                    if (row.i == 0)
                    {
                        agent = new RecordAgent(record[0], record[1], record[2]);
                    }
                    else if (row.i == 1)
                    {
                        if (record[0] == "delete")
                        {
                            agent.SetDelete(record[2]);
                        }
                    }
                    else
                    {
                        agent.SetRecord(record);
                        agent.Record.Context = entities;
                        agent.ExecuteProcess();
                    }
                }
            }
        }
    }
}
