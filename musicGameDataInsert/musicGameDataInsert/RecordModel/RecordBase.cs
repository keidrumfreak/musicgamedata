﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicGameDataInsert.DataEntity;

namespace musicGameDataInsert.RecordModel
{
    abstract class RecordBase
    {
        public MusicGameData Context { get; set; }

        public string MusicName { get; set; }
        public string ArtistName { get; set; }
        public DateTime? AdditionDate { get; set; }

        public int MusicId { get; set; }
        public int ArtistId { get; set; }
        public int TrackId { get; set; }

        protected RecordBase(string musicName, string artistName, string addtionDate)
        {
            MusicName = musicName;
            ArtistName = artistName;
            AdditionDate = Utility.StringToDateTime(addtionDate);
        }

        public virtual void ExcuteProcess(Title title)
        {
            ArtistId = getArtistId(ArtistName);
            MusicId = getMusicId(MusicName, ArtistId);
            TrackId = getTrackId(title, MusicId);
        }

        private int getArtistId(string name)
        {
            Func<Artist, bool> expression = a => a.Name == name;

            var artist = Context.Artists.Where(expression);

            if (artist.Count() != 0)
            {
                return artist.ToArray()[0].Id;
            }

            Context.Artists.Add(new Artist { Name = name });
            Context.SaveChanges();

            return Context.Artists.Where(expression).ToArray()[0].Id;
        }

        private int getMusicId(string name, int artistId)
        {
            Func<Music, bool> expression = m => m.Name == name && m.ArtistId == artistId;

            var music = Context.Musics.Where(expression);

            if (music.Count() != 0)
            {
                return music.ToArray()[0].Id;
            }

            Context.Musics.Add(new Music { Name = name, ArtistId = artistId });
            Context.SaveChanges();

            return Context.Musics.Where(expression).ToArray()[0].Id;
        }

        private int getTrackId(Title title, int musicId)
        {
            Func<Track, bool> expression = t => t.TitleId == title.Id && t.MusicId == musicId;

            var track = Context.Tracks.Where(expression);

            if (track.Count() != 0)
            {
                return track.ToArray()[0].Id;
            }

            DateTime? date;

            if (AdditionDate == null)
            {
                date = title.RollDate;
            }
            else
            {
                date = AdditionDate;
            }

            Context.Tracks.Add(new Track { TitleId = title.Id, MusicId = musicId, AdditionDate = date });
            Context.SaveChanges();

            return Context.Tracks.Where(expression).ToArray()[0].Id;
        }

        public int GetStageId(Title title, int difficultyRank, int musicId, int notes, decimal?[] bpm)
        {
            var difficultyId = Context.Difficulties.Where(d => d.DifficultyTableId == title.DifficultyTableId && d.Rank == difficultyRank)
                .ToArray()[0].Id;

            Func<Stage, bool> expression = s => s.Equals(title.ModelId, musicId, difficultyId, notes, bpm);

            var stage = Context.Stages.Where(expression);

            if (stage.Count() != 0)
            {
                return stage.ToArray()[0].Id;
            }

            Context.Stages.Add(
                    new Stage
                    {
                        MusicId = musicId,
                        DifficultyId = difficultyId,
                        Notes = notes,
                        DefaultBpm = bpm[0],
                        MinBpm = bpm[1],
                        MaxBpm = bpm[2]
                    }
                    );
            Context.SaveChanges();

            return Context.Stages.Where(expression).ToArray()[0].Id;
        }
    }
}
