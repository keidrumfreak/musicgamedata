﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("music")]
    class Music
    {
        [Key]
        [Column("music_id")]
        public int Id { get; set; }

        [Column("music_name")]
        public string Name { get; set; }

        [Column("artist_id")]
        public int ArtistId { get; set; }

        [ForeignKey(nameof(ArtistId))]
        public Artist Artist { get; set; }
    }
}
