﻿CREATE TABLE "musicGameData".artist
(
  artist_id serial NOT NULL,
  artist_name text NOT NULL,
  CONSTRAINT artist_pkey PRIMARY KEY (artist_id)
);

CREATE TABLE "musicGameData".music
(
  music_id serial NOT NULL,
  music_name text NOT NULL,
  artist_id integer NOT NULL,
  CONSTRAINT music_pkey PRIMARY KEY (music_id),
  CONSTRAINT music_artist_id_fkey FOREIGN KEY (artist_id)
      REFERENCES "musicGameData".artist (artist_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "musicGameData".model
(
  model_id serial NOT NULL,
  model_name text NOT NULL,
  CONSTRAINT model_pkey PRIMARY KEY (model_id)
);

CREATE TABLE "musicGameData".location
(
  location_id serial NOT NULL,
  location_name text NOT NULL,
  CONSTRAINT location_pkey PRIMARY KEY (location_id)
);

CREATE TABLE "musicGameData".difficulty_table
(
  difficulty_table_id serial NOT NULL,
  model_id integer NOT NULL,
  CONSTRAINT difficulty_table_pkey PRIMARY KEY (difficulty_table_id),
  CONSTRAINT difficulty_table_model_id_fkey FOREIGN KEY (model_id)
      REFERENCES "musicGameData".model (model_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "musicGameData".difficulty
(
  difficulty_id serial NOT NULL,
  difficulty_table_id integer NOT NULL,
  difficulty_rank integer NOT NULL,
  difficulty_name text NOT NULL,
  CONSTRAINT difficulty_pkey PRIMARY KEY (difficulty_id),
  CONSTRAINT difficulty_difficulty_table_id_fkey FOREIGN KEY (difficulty_table_id)
      REFERENCES "musicGameData".difficulty_table (difficulty_table_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT difficulty_difficulty_table_id_difficulty_rank_key UNIQUE (difficulty_table_id, difficulty_rank)
);

CREATE TABLE "musicGameData".title
(
  title_id serial NOT NULL,
  model_id integer NOT NULL,
  location_id integer NOT NULL,
  version_no numeric(4,1) NOT NULL,
  title_name text NOT NULL,
  roll_date date NOT NULL,
  difficulty_table_id integer NOT NULL,
  CONSTRAINT title_pkey PRIMARY KEY (title_id),
  CONSTRAINT title_difficulty_table_id_fkey FOREIGN KEY (difficulty_table_id)
      REFERENCES "musicGameData".difficulty_table (difficulty_table_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT title_location_id_fkey FOREIGN KEY (location_id)
      REFERENCES "musicGameData".location (location_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT title_model_id_fkey FOREIGN KEY (model_id)
      REFERENCES "musicGameData".model (model_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT title_model_id_location_id_version_no_key UNIQUE (model_id, location_id, version_no)
);

CREATE TABLE "musicGameData".stage
(
  stage_id serial NOT NULL,
  difficulty_id integer NOT NULL,
  music_id integer NOT NULL,
  notes integer NOT NULL,
  bpm_default numeric(5,1) NOT NULL,
  bpm_max numeric(5,1),
  bpm_min numeric(5,1),
  CONSTRAINT stage_pkey PRIMARY KEY (stage_id),
  CONSTRAINT stage_difficulty_id_fkey FOREIGN KEY (difficulty_id)
      REFERENCES "musicGameData".difficulty (difficulty_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stage_music_id_fkey FOREIGN KEY (music_id)
      REFERENCES "musicGameData".music (music_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "musicGameData".track
(
  track_id serial NOT NULL,
  title_id integer NOT NULL,
  music_id integer NOT NULL,
  addition_date date,
  delete_date date,
  CONSTRAINT track_pkey PRIMARY KEY (track_id),
  CONSTRAINT track_music_id_fkey FOREIGN KEY (music_id)
      REFERENCES "musicGameData".music (music_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT track_title_id_fkey FOREIGN KEY (title_id)
      REFERENCES "musicGameData".title (title_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "musicGameData".stage_level
(
  stage_level_id serial NOT NULL,
  track_id integer NOT NULL,
  stage_id integer NOT NULL,
  difficulty_level text NOT NULL,
  CONSTRAINT stage_level_pkey PRIMARY KEY (stage_level_id),
  CONSTRAINT stage_level_stage_id_fkey FOREIGN KEY (stage_id)
      REFERENCES "musicGameData".stage (stage_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stage_level_track_id_fkey FOREIGN KEY (track_id)
      REFERENCES "musicGameData".track (track_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
