﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("title")]
    class Title
    {
        [Key]
        [Column("title_id")]
        public int Id { get; set; }

        [Column("model_id")]
        public int ModelId { get; set; }

        [Column("location_id")]
        public int LocationId { get; set; }

        [Column("version_no")]
        public decimal VersionNo { get; set; }

        [Column("title_name")]
        public string Name { get; set; }

        [Column("roll_date")]
        public DateTime RollDate { get; set; }

        [Column("difficulty_table_id")]
        public int DifficultyTableId { get; set; }

        [ForeignKey(nameof(ModelId))]
        public Model Model { get; set; }

        [ForeignKey(nameof(LocationId))]
        public Location Location { get; set; }

        [ForeignKey(nameof(DifficultyTableId))]
        public DifficultyTable DifficultyTable { get; set; }
    }
}
