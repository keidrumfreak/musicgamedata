﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("artist")]
    class Artist
    {
        [Key]
        [Column("artist_id")]
        public int Id { get; set; }

        [Column("artist_name")]
        public string Name { get; set; }
    }
}
