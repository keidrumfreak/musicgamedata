﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("model")]
    class Model
    {
        [Key]
        [Column("model_id")]
        public int Id { get; set; }

        [Column("model_name")]
        public string Name { get; set; }
    }
}
