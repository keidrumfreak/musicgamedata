﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicGameDataInsert.DataEntity;

namespace musicGameDataInsert.RecordModel
{
    class DeleteRecord : RecordBase
    {
        public DateTime? DeleteDate { get; set; }
        public Title Title { get; set; }

        public DeleteRecord(Title title, string[] record)
            : base(record[0], record[1], null)
        {
            Title = title;

            if (string.IsNullOrEmpty(record[2]))
            {
                DeleteDate = null;
            }
            else
            {
                DeleteDate = Convert.ToDateTime(record[2]);
            }
        }

        public override void ExcuteProcess(Title title)
        {
            base.ExcuteProcess(Title);

            var track = Context.Tracks.Where(t => t.Id == TrackId).ToArray()[0];

            if (DeleteDate == null)
            {
                DeleteDate = title.RollDate;
            }

            track.DeleteDate = DeleteDate;
            Context.SaveChanges();
        }
    }
}
