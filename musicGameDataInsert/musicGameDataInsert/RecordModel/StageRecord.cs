﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicGameDataInsert.DataEntity;

namespace musicGameDataInsert.RecordModel
{
    class StageRecord
    {
        public RecordBase Parent { get; set; }
        public int DifficultyRank { get; set; }
        public string Difficulty { get; set; }
        public int Notes { get; set; }
        public decimal?[] Bpm { get; set; }

        public StageRecord(RecordBase record, int difficultyRank, string difficulty, int notes, decimal?[] bpm)
        {
            Parent = record;
            DifficultyRank = difficultyRank;
            Difficulty = difficulty;
            Notes = notes;
            Bpm = bpm;
        }

        public void ExecuteInsert(Title title)
        {
            var stageId = Parent.GetStageId(title, DifficultyRank, Parent.MusicId, Notes, Bpm);
            Func<StageLevel, bool> expression = s => s.TrackId == Parent.TrackId && s.StageId == stageId;

            var stageLevel = Parent.Context.StageLevels.Where(expression);
            if (stageLevel.Count() != 0)
            {
                return;
            }

            var difficultyId = Parent.Context.Difficulties.Where(d => d.DifficultyTableId == title.DifficultyTableId && d.Rank == DifficultyRank)
                .Select(d => d.Id).ToArray()[0];

            Parent.Context.StageLevels.Add(
                    new StageLevel
                    {
                        TrackId = Parent.TrackId,
                        StageId = stageId,
                        Level = Difficulty
                    }
                    );
            Parent.Context.SaveChanges();
        }
    }
}
