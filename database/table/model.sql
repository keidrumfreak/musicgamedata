﻿INSERT INTO location (location_name) VALUES ('AC');
INSERT INTO location (location_name) VALUES ('CS');

INSERT INTO model (model_name) VALUES ('beatmania IIDX');
INSERT INTO difficulty_table (difficulty_table_id, model_id) VALUES (1, 1);
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 0, 'BIGGINNER');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 1, 'LIGHT7');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 2, '7KEYS');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 3, 'ANOTHER(7)');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 4, 'LIGHT14');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 5, '14KEYS');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (1, 6, 'ANOTHER(14)');
INSERT INTO difficulty_table (difficulty_table_id, model_id) VALUES (2, 1);
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 0, 'SP BIGGINNER');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 1, 'SP NORMAL');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 2, 'SP HYPER');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 3, 'SP ANOTHER');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 4, 'DP NORMAL');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 5, 'DP HYPER');
INSERT INTO difficulty (difficulty_table_id, difficulty_rank, difficulty_name) VALUES (2, 6, 'DP ANOTHER');

INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 1, '', '1999-2-26', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 1.5, 'substream', '1999-7-27', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 2, '2nd style', '1999-9-30', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 3, '3rd style', '2000-2-25', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 4, '4th style', '2000-9-28', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 5, '5th style', '2001-3-27', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 6, '6th style', '2001-9-28', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 7, '7th style', '2002-3-27', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 8, '8th style', '2002-9-27', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 9, '9th style', '2003-6-25', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 10, '10th style', '2004-2-18', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 11, 'IIDX RED', '2004-10-28', 1);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 12, 'HAPPY SKY', '2005-7-13', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 13, 'DistorteD', '2006-3-15', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 14, 'GOLD', '2007-2-21', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 15, 'DJ TROOPERS', '2007-12-19', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 16, 'EMPRESS', '2008-11-19', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 17, 'SIRIUS', '2009-10-21', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 18, 'Resort Anthem', '2010-9-15', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 19, 'Licle', '2011-9-15', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 20, 'tricoro', '2012-9-19', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 21, 'SPADA', '2013-11-13', 2);
INSERT INTO title (model_id, location_id, version_no, title_name, roll_date, difficulty_table_id)
	VALUES (1, 1, 22, 'PENDUAL', '2014-9-17', 2);
