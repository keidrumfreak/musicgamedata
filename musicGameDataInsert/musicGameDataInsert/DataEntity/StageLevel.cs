﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("stage_level")]
    class StageLevel
    {
        [Key]
        [Column("stage_level_id")]
        public int Id { get; set; }

        [Column("stage_id")]
        public int StageId { get; set; }

        [Column("track_id")]
        public int TrackId { get; set; }

        [Column("difficulty_level")]
        public string Level { get; set; }

        [ForeignKey(nameof(StageId))]
        public Stage Stage { get; set; }

        [ForeignKey(nameof(TrackId))]
        public Track Track { get; set; }
    }
}
