﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using musicGameDataInsert.DataEntity;

namespace musicGameDataInsert.RecordModel
{
    class RecordAgent
    {
        public Title Title { get; private set; }
        public RecordBase Record { get; set; }

        public Title PreviousTitle { get; private set; }

        public RecordAgent(string modelName, string locationName, string titleName)
        {
            using (var entities = new MusicGameData())
            {
                Title = entities.Titles.Where(
                    v => v.Model.Name == modelName && v.Location.Name == locationName && v.Name == titleName).ToArray()[0];
            }
        }

        public void SetDelete(string titleName)
        {
            using (var entities = new MusicGameData())
            {
                PreviousTitle = entities.Titles.Where(
                    v => v.ModelId == Title.ModelId && v.LocationId == Title.LocationId && v.Name == titleName).ToArray()[0];
            }
        }

        public void SetRecord(string[] record)
        {
            if (PreviousTitle != null)
            {
                Record = new DeleteRecord(PreviousTitle, record);
                return;
            }

            switch(Title.DifficultyTableId)
            {
                case 1: // IIDX classic
                    Record = new IidxClassic(record);
                    break;
            }
        }

        public void ExecuteProcess()
        {
            using (var tran = Record.Context.Database.BeginTransaction())
            {
                try
                {
                    Record.ExcuteProcess(Title);
                    tran.Commit();
                }
                catch (Exception)
                {
                    tran.Rollback();
                    throw;
                }
            }
        }

        private void executeDelete()
        {
            var x = Record.Context.Tracks.Where(tr =>
            tr.TitleId == Record.Context.Titles.Where(t => t.ModelId == Title.ModelId && t.RollDate < Title.RollDate)
                .OrderBy(t => t.RollDate).Last().Id);
            var b = x.Where(t => !x.Any(tr => t.MusicId == tr.MusicId));
        }
    }
}
