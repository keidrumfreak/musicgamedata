﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("track")]
    class Track
    {
        [Key]
        [Column("track_id")]
        public int Id { get; set; }

        [Column("title_id")]
        public int TitleId { get; set; }

        [Column("music_id")]
        public int MusicId { get; set; }

        [Column("addition_date")]
        public DateTime? AdditionDate { get; set; }

        [Column("delete_date")]
        public DateTime? DeleteDate { get; set; }

        [ForeignKey(nameof(TitleId))]
        public Title Title { get; set; }

        [ForeignKey(nameof(MusicId))]
        public Music Music { get; set; }
    }
}
