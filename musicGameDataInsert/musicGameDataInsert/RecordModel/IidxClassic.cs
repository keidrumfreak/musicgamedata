﻿using System;
using System.Linq;
using musicGameDataInsert.DataEntity;

namespace musicGameDataInsert.RecordModel
{
    class IidxClassic : RecordBase
    {
        public StageRecord LightSeven { get; set; }
        public StageRecord SevenKeys { get; set; }
        public StageRecord AnotherSeven { get; set; }

        public StageRecord LightFourteen { get; set; }
        public StageRecord FourteenKeys { get; set; }
        public StageRecord AnotherFourteen { get; set; }

        public IidxClassic(string[] record)
            : base(record[0], record[2], record[33])
        {
            LightSeven = createSubstance(1, record, 3);
            SevenKeys = createSubstance(2, record, 8);
            AnotherSeven = createSubstance(3, record, 13);
            LightFourteen = createSubstance(4, record, 18);
            FourteenKeys = createSubstance(5, record, 23);
            AnotherFourteen = createSubstance(6, record, 28);
        }

        private StageRecord createSubstance(int rank, string[] value, int index)
        {
            if (string.IsNullOrEmpty(value[index]))
            {
                return null;
            }

            string difficulty = value[index];
            int notes = int.Parse(value[index + 1]);
            decimal?[] bpm = Array.ConvertAll(new string[] { value[index + 2], value[index + 3], value[index + 4] }, Utility.StringToDecimal);

            return new StageRecord(this, rank, difficulty, notes, bpm);
        }

        public override void ExcuteProcess(Title title)
        {
            base.ExcuteProcess(title);

            LightSeven?.ExecuteInsert(title);
            SevenKeys?.ExecuteInsert(title);
            AnotherSeven?.ExecuteInsert(title);
            LightFourteen?.ExecuteInsert(title);
            FourteenKeys?.ExecuteInsert(title);
            AnotherFourteen?.ExecuteInsert(title);
        }
    }
}
