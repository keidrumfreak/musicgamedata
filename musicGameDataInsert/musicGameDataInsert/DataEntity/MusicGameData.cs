﻿using System;
using System.IO;
using System.Data.Entity;

namespace musicGameDataInsert.DataEntity
{
    class MusicGameData : DbContext
    {
        public MusicGameData()
            : base("name = musicGameData")
        {
            Database.Log = loging;
        }

        void loging(string input)
        {
            using (var writer = new StreamWriter("log.txt", true))
            {
                writer.WriteLine(input);
            }
            Console.WriteLine(input);
        }

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Music> Musics { get; set; }

        public DbSet<Location> Locations { get; set; }
        public DbSet<Model> Models { get; set; }

        public DbSet<DifficultyTable> DifficultyTables { get; set; }
        public DbSet<Difficulty> Difficulties { get; set; }

        public DbSet<Title> Titles { get; set; }
        public DbSet<Track> Tracks { get; set; }

        public DbSet<Stage> Stages { get; set; }
        public DbSet<StageLevel> StageLevels { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("musicGameData");
        }
    }
}
