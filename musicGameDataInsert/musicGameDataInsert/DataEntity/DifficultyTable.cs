﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("difficulty_table")]
    class DifficultyTable
    {
        [Key]
        [Column("difficulty_table_id")]
        public int Id { get; set; }

        [Column("model_id")]
        public int ModelId { get; set; }

        [ForeignKey(nameof(ModelId))]
        public Model Model { get; set; }
    }
}
