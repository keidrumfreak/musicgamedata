﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace musicGameDataInsert.DataEntity
{
    [Table("difficulty")]
    class Difficulty
    {
        [Key]
        [Column("difficulty_id")]
        public int Id { get; set; }

        [Column("difficulty_table_id")]
        public int DifficultyTableId { get; set; }

        [Column("difficulty_rank")]
        public int Rank { get; set; }

        [Column("difficulty_name")]
        public string Name { get; set; }

        [ForeignKey(nameof(DifficultyTableId))]
        public DifficultyTable DifficultyTable { get; set; }
    }
}
